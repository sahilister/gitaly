#!/bin/sh

set -e

# Read gitlab_user from gitlab-common.conf
if [ -f /etc/gitlab-common/gitlab-common.conf ]; then
  . /etc/gitlab-common/gitlab-common.conf
else
  echo "Failed to find /etc/gitlab-common/gitlab-common.conf."
  exit 1
fi
gitaly_ruby_home=/usr/share/gitaly/ruby

# Make gitlab user owner of .bundle for gitaly
chown ${gitlab_user}: /var/lib/gitlab/gitaly-ruby.bundle
# Regenerate Gemfile.lock
cd ${gitaly_ruby_home}
runuser -u ${gitlab_user} -- sh -c "touch Gemfile.lock && \
truncate -s 0 Gemfile.lock"
runuser -u ${gitlab_user} -- sh -c "bundle install --local"
if ! runuser -u ${gitlab_user} -- sh -c 'bundle --local --quiet'; then
  if [ "$1" = "triggered" ]; then
    # probably triggered in the middle of an system upgrade; ignore failure
    # but abort here
    echo "#########################################################################"
    echo "# Failed to detect gitaly dependencies; if you are in the middle of an #"
    echo "# upgrade, this is probably fine, there will be another attempt later.  #"
    echo "#                                                                       #"
    echo "# If you are NOT in the middle of an upgrade, there is probably a real  #"
    echo "# issue. Please report a bug.                                           #"
    echo "#########################################################################"
    exit 0
  else
    # something is really broken
    exit 1
  fi
fi

case $1 in
    configure)
      service_path=/etc/systemd/system/gitaly.service.d
      mkdir -p ${service_path}
      if [ -e ${service_path}/override.conf ]; then
        echo "${service_path}/override.conf already exist"
        # Make sure only gitlab user is updated
        sed -i "s/^ *User=.*/User=${gitlab_user}/" ${service_path}/override.conf
      else
        printf "[Service]\nUser=${gitlab_user}\n" > ${service_path}/override.conf
      fi
      # Check if storage path exist and create if required
      export $(grep '^\s*path\s*=' /etc/gitaly/config.toml | sed 's/ //g' | sed 's/"//g')
      if ! [ -d $path ] ; then runuser -u ${gitlab_user} -- sh -c 'mkdir $path'; fi
    ;;

    triggered)
    # Already handled
    ;;

    abort-upgrade|abort-remove|abort-deconfigure)
    ;;
    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

#DEBHELPER#
